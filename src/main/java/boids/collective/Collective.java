package boids.collective;

import boids.Boid;
import boids.IBoidImmutable;
import boids.BoidMutable;
import boids.meta.OrderTag;
import boids.meta.Scale;
import boids.meta.SpatialBucketKey;
import boids.mind.Order;
import concurrent.delayed.delayed_collection.IDelayedCollection;
import concurrent.delayed.delayed_collection.multiple_owner.DelayedMultipleOwnerConcurrentLinkedQueue;
import concurrent.delayed.delayed_map.IDelayedMap;
import concurrent.delayed.delayed_map.multiple_owner.DelayedMultipleOwnerConcurrentHashMap;
import simulation.Simulation;
import simulation.Updatable;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;

public class Collective extends Updatable
{
    private final Simulation simulation;

    public Collective(Simulation _simulation)
    {
        simulation = _simulation;
    }

    //#region boids

    private final IDelayedCollection<IBoidImmutable> boidsDelayedCollection = new DelayedMultipleOwnerConcurrentLinkedQueue<>();

    public BoidMutable createBoid(double _x, double _y, double _turnPower, int _team, Scale _scale)
    {
        BoidMutable boid = new Boid(_x, _y, _turnPower, _team, _scale, this, simulation.config);

        simulation.queueAddingUpdatable(boid);
        boidsDelayedCollection.queueAddingElement(boid);

        return boid;
    }

    private volatile List<IBoidImmutable> safeBoidList = Arrays.asList(new IBoidImmutable[0]);

    public List<IBoidImmutable> getAllBoids()
    {
        return safeBoidList;
    }

    //#endregion

    //region orders

    private final IDelayedMap<OrderTag, Order> orderTagsDelayedMap = new DelayedMultipleOwnerConcurrentHashMap<>();

    public Order getOrderByOrderTag(OrderTag _orderTag)
    {
        return orderTagsDelayedMap.getMapUnsafe().get(_orderTag);
    }

    public void queueChangingOrder(OrderTag _orderTag, Order _order)
    {
        orderTagsDelayedMap.queueKVChange(_orderTag, _order);
    }

    //endregion

    //region organize boids

    private ConcurrentMap<SpatialBucketKey, Queue<IBoidImmutable>> bucketMap = new ConcurrentHashMap<>();

    private void organizeBoids()
    {
        Collection<IBoidImmutable> boidCollection = boidsDelayedCollection.getCollectionUnsafe();
        IBoidImmutable[] boidArray = new IBoidImmutable[boidCollection.size()];

        bucketMap = new ConcurrentHashMap<>();

        int i = 0;
        for (IBoidImmutable boid : boidCollection)
        {
            boidArray[i] = boid;
            i++;

            SpatialBucketKey key = boid.getSpatialBucketKey();

            Queue<IBoidImmutable> currentBucket;

            if (bucketMap.containsKey(key))
            {
                currentBucket = bucketMap.get(key);
                currentBucket.add(boid);
                bucketMap.put(key, currentBucket);
            } else
            {
                currentBucket = new ConcurrentLinkedQueue<>();
                currentBucket.add(boid);
                bucketMap.put(key, currentBucket);
            }
        }

        safeBoidList = Arrays.asList(boidArray);
    }

    public List<Queue<IBoidImmutable>> getLocalBoids(IBoidImmutable _boid)
    {
        List<SpatialBucketKey> keys = _boid.getNeighboringSpatialBucketKeys();

        List<Queue<IBoidImmutable>> buckets = new ArrayList<>();

        for (SpatialBucketKey key : keys)
        {
            Queue<IBoidImmutable> currentBucket = bucketMap.get(key);
            if (currentBucket != null)
            {
                buckets.add(currentBucket);
            }
        }

        return buckets;
    }

    //endregion

    //region updatables

    @Override
    public void update()
    {
        boidsDelayedCollection.executeWrites();
        orderTagsDelayedMap.executeKVChanges();
        organizeBoids();
    }

    //endregion
}
