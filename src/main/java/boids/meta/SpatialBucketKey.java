package boids.meta;

import vectors.ImmutableVector2D;

import java.util.ArrayList;
import java.util.List;

public class SpatialBucketKey
{
    public final int xCoord;
    public final int yCoord;
    public final Scale scale;

    public SpatialBucketKey(ImmutableVector2D _pos, Scale _scale)
    {
        xCoord = (int) Math.floor(_pos.x / _scale.chunkSize);
        yCoord = (int) Math.floor(_pos.y / _scale.chunkSize);
        scale = _scale;
    }

    private SpatialBucketKey(int _xCoord, int _yCoord, Scale _scale)
    {
        xCoord = _xCoord;
        yCoord = _yCoord;
        scale = _scale;
    }

    public List<SpatialBucketKey> getNeighboringKeys()
    {
        List<SpatialBucketKey> keys = new ArrayList<>(9);

        keys.add(new SpatialBucketKey(xCoord - 1, yCoord - 1, scale));
        keys.add(new SpatialBucketKey(xCoord, yCoord - 1, scale));
        keys.add(new SpatialBucketKey(xCoord + 1, yCoord - 1, scale));
        keys.add(new SpatialBucketKey(xCoord + 1, yCoord, scale));
        keys.add(new SpatialBucketKey(xCoord + 1, yCoord + 1, scale));
        keys.add(new SpatialBucketKey(xCoord, yCoord + 1, scale));
        keys.add(new SpatialBucketKey(xCoord - 1, yCoord + 1, scale));
        keys.add(new SpatialBucketKey(xCoord - 1, yCoord, scale));
        keys.add(this);

        return keys;
    }

    @Override
    public String toString()
    {
        return "SpatialBuckeyKey Coords: (" + xCoord + ", " + yCoord + ") SpatialBucketKey scale: " + scale.name();
    }

    @Override
    public boolean equals(Object _o)
    {
        if (_o == null)
        {
            return false;
        }

        if (!(_o instanceof SpatialBucketKey))
        {
            return false;
        }

        SpatialBucketKey o = (SpatialBucketKey) _o;

        boolean xCoordCheck = xCoord == o.xCoord;
        boolean yCoordCheck = yCoord == o.yCoord;
        boolean scaleCheck = scale == o.scale;

        return xCoordCheck && yCoordCheck && scaleCheck;
    }

    @Override
    public int hashCode()
    {
        //using prime numbers on different orders of magnitude to prevent collisions
        int core1 = 17 * xCoord;
        int core2 = 7919 * yCoord;
        int core3 = 51859 * scale.size;

        return core1 + core2 + core3;
    }
}
