package boids.meta;

public enum Scale
{
    PICO(1),
    NANO(2),
    MICRO(3),
    MILLI(4),
    CENTI(5),
    KILO(6),
    MEGA(7),
    GIGA(8),
    TERA(9),
    PETA(10);

    public final int size;
    public final int chunkSize;

    Scale(int _size)
    {
        size = _size;
        chunkSize = (int) Math.pow(2, 4 + size);
    }
}
