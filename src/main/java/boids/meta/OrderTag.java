package boids.meta;

public class OrderTag
{
    public final String name;
    public final int team;
    public final Scale scale;

    public OrderTag(String _name, int _team, Scale _scale)
    {
        name = _name;
        team = _team;
        scale = _scale;
    }

    @Override
    public String toString()
    {
        return "OrderTag name: " + name + " OrderTag team: " + team + " OrderTag scale: " + scale.size;
    }

    @Override
    public boolean equals(Object _o)
    {
        if (_o == null)
        {
            return false;
        }

        if (!(_o instanceof OrderTag))
        {
            return false;
        }

        OrderTag o = (OrderTag) _o;

        boolean nameCheck = name.equals(o.name);
        boolean teamCheck = team == o.team;
        boolean scaleCheck = scale == o.scale;

        return nameCheck && teamCheck && scaleCheck;
    }

    @Override
    public int hashCode()
    {
        //using prime numbers on different orders of magnitude to prevent collisions
        int core1 = name.hashCode();
        int core2 = 7919 * team;
        int core3 = 51859 * scale.size;

        return core1 + core2 + core3;
    }
}
