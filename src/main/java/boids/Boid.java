package boids;

import boids.collective.Collective;
import boids.location_tracker.IPositionTrackerImmutable;
import boids.location_tracker.IPositionTrackerMutable;
import boids.location_tracker.PositionTracker;
import boids.meta.OrderTag;
import boids.meta.Scale;
import boids.meta.SpatialBucketKey;
import boids.mind.Order;
import boids.mind.OrderExecutor;
import vectors.ComputingVector2D;
import vectors.ImmutableVector2D;
import simulation.SimulationConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;

public class Boid extends BoidMutable
{
    private final SimulationConfig config;
    private final Collective collective;

    private final IPositionTrackerMutable positionTracker;

    private volatile ImmutableVector2D impulse;

    private final int team;
    private final Scale scale;
    private final List<OrderTag> orderTags = new CopyOnWriteArrayList<>();

    private final double space;
    private final double vision;

    private final double space2;
    private final double vision2;

    public Boid(double _x, double _y, double _turnPower, int _team, Scale _scale, Collective _collective, SimulationConfig _config)
    {
        positionTracker = new PositionTracker(new ImmutableVector2D(_x, _y), _turnPower, _config);

        impulse = ImmutableVector2D.ZERO_VECTOR;

        team = _team;
        scale = _scale;

        space = _scale.chunkSize * .8;
        vision = _scale.chunkSize * 1;

        space2 = space * space;
        vision2 = vision * vision;

        collective = _collective;

        config = _config;
    }

    @Override
    public IPositionTrackerImmutable getPositionTracker()
    {
        return positionTracker;
    }

    @Override
    public double getSpace()
    {
        return space;
    }

    @Override
    public double getVision()
    {
        return vision;
    }

    @Override
    public int getTeam()
    {
        return team;
    }

    //region Order Tags
    @Override
    public List<OrderTag> getOrderTags()
    {
        return new ArrayList<>(orderTags);
    }

    @Override
    public synchronized void addOrderTag(OrderTag _orderTag)
    {
        if(_orderTag.team != team || _orderTag.scale != scale || orderTags.contains(_orderTag))
        {
            return;
        }

        orderTags.add(_orderTag);
    }

    @Override
    public synchronized void removeOrderTag(OrderTag _orderTag)
    {
        orderTags.remove(_orderTag);
    }
    //endregion

    //region SpatialBucketKeys
    @Override
    public SpatialBucketKey getSpatialBucketKey()
    {
        return new SpatialBucketKey(positionTracker.getPosition(), scale);
    }

    @Override
    public List<SpatialBucketKey> getNeighboringSpatialBucketKeys()
    {
        return getSpatialBucketKey().getNeighboringKeys();
        //return SpatialBucketKey.getNeighboringKeys(positionTracker.getPosition(), scale);
    }
    //endregion

    @Override
    public SimulationConfig getConfig()
    {
        return config;
    }

    //region updatables
    @Override
    protected synchronized void updateInit()
    {
        {
            positionTracker.addImpulseToVelocity(impulse, config.maxSpeedTimesDeltaTime);
            positionTracker.addVelocityToPosition();

            impulse = ImmutableVector2D.ZERO_VECTOR;
        }
    }

    @Override
    protected synchronized void updateFinish()
    {
        List<OrderExecutor> orderExecutors = new ArrayList<>();

        for (OrderTag orderTag : orderTags)
        {
            Order order = collective.getOrderByOrderTag(orderTag);

            if (order != null)
            {
                orderExecutors.add(order.getExecutor());
            }
        }

        List<Queue<IBoidImmutable>> boidBuckets = collective.getLocalBoids(this);

        for (Queue<IBoidImmutable> bucket : boidBuckets)
        {
            for (IBoidImmutable counter : bucket)
            {
                double distance2 = positionTracker.dst2(counter.getPositionTracker());

                if(distance2 > vision2)
                {
                    continue;
                }

                double distance = Math.sqrt(distance2);

                for (OrderExecutor orderExecutor : orderExecutors)
                {
                    orderExecutor.executeCycle(this, counter, distance, config);
                }
            }
        }

        for (OrderExecutor orderExecutor : orderExecutors)
        {
            ComputingVector2D executorImpulse = orderExecutor.executePostCycle(this, config);

            ImmutableVector2D newImpulse = executorImpulse.add(impulse).endCompute();
            impulse = newImpulse;
        }
    }
    //endregion
}
