package boids;

import boids.meta.OrderTag;
import simulation.Updatable;

public abstract class BoidMutable extends Updatable implements IBoidImmutable
{
    abstract public void addOrderTag(OrderTag _tag);

    abstract public void removeOrderTag(OrderTag _tag);
}
