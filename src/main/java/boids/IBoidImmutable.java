package boids;

import boids.location_tracker.IPositionTrackerImmutable;
import boids.meta.OrderTag;
import boids.meta.SpatialBucketKey;
import simulation.SimulationConfig;

import java.util.List;

public interface IBoidImmutable
{
    IPositionTrackerImmutable getPositionTracker();

    double getSpace();

    double getVision();

    int getTeam();

    List<OrderTag> getOrderTags();

    SpatialBucketKey getSpatialBucketKey();

    List<SpatialBucketKey> getNeighboringSpatialBucketKeys();

    SimulationConfig getConfig();
}
