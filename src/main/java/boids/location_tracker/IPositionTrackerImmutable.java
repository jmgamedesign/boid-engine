package boids.location_tracker;

import vectors.ImmutableVector2D;

public interface IPositionTrackerImmutable
{
    ImmutableVector2D getPosition();

    ImmutableVector2D getVelocity();

    ImmutableVector2D getAngleDir();

    ImmutableVector2D getPositionAtPlusNTicks(int _ticks);

    double dst(IPositionTrackerImmutable _counter);

    double dst2(IPositionTrackerImmutable _counter);
}
