package boids.location_tracker;

import vectors.ImmutableVector2D;

public interface IPositionTrackerMutable extends IPositionTrackerImmutable
{
    void setPosition(ImmutableVector2D _position);

    void addImpulseToVelocity(ImmutableVector2D _impulse, double _maxSpeed);

    void setVelocity(ImmutableVector2D _velocity);

    void addVelocityToPosition();
}
