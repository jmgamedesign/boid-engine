package boids.location_tracker;

import simulation.SimulationConfig;
import vectors.ComputingVector2D;
import vectors.ImmutableVector2D;

public class PositionTracker implements IPositionTrackerMutable
{
    private volatile ImmutableVector2D position;
    private volatile ImmutableVector2D previousPosition;
    private volatile ImmutableVector2D velocity;
    private volatile ImmutableVector2D angleDir;

    private final SimulationConfig config;
    private final double maxSpeedTimesDeltaTime;
    private final double turnPower;

    public PositionTracker(ImmutableVector2D _position, SimulationConfig _config)
    {
        this(_position, 100, _config);
    }

    public PositionTracker(ImmutableVector2D _position, double _turnPower, SimulationConfig _config)
    {
        position = _position;
        previousPosition = position;
        velocity = ImmutableVector2D.ZERO_VECTOR;
        angleDir = ImmutableVector2D.ZERO_VECTOR;

        config = _config;
        maxSpeedTimesDeltaTime = _config.maxSpeedTimesDeltaTime;
        turnPower = ((_turnPower * 2) / 100) * config.deltaTime;
    }

    @Override
    public synchronized void setPosition(ImmutableVector2D _position)
    {
        previousPosition = position;
        position = _position;

        ComputingVector2D computeVelocity = position.beginCompute().sub(previousPosition);
        velocity = computeVelocity.endCompute();

        computeVelocity.unitVector().mul(turnPower);
        ComputingVector2D computeAngleDir = angleDir.beginCompute().add(computeVelocity).unitVector();
        angleDir = computeAngleDir.endCompute();
    }

    @Override
    public void addImpulseToVelocity(ImmutableVector2D _impulse, double _maxSpeedTimesDeltaTime)
    {
        double maxSpeed = maxSpeedTimesDeltaTime;

        if(maxSpeed > _maxSpeedTimesDeltaTime)
        {
            maxSpeed = _maxSpeedTimesDeltaTime;
        }

        ImmutableVector2D tempVelocity = velocity.beginCompute().add(_impulse).limit(maxSpeed).endCompute();
        setVelocity(tempVelocity);
    }

    @Override
    public synchronized void setVelocity(ImmutableVector2D _velocity)
    {
        velocity = _velocity;
    }

    @Override
    public synchronized void addVelocityToPosition()
    {
        setPosition(position.beginCompute().add(velocity).endCompute());
    }

    @Override
    public ImmutableVector2D getPosition()
    {
        return position;
    }

    @Override
    public ImmutableVector2D getVelocity()
    {
        return velocity;
    }

    @Override
    public ImmutableVector2D getAngleDir()
    {
        return angleDir;
    }

    @Override
    public ImmutableVector2D getPositionAtPlusNTicks(int _ticks)
    {
        ComputingVector2D nTickVelocity = velocity.beginCompute().mul(_ticks);

        return nTickVelocity.add(position).endCompute();
    }

    @Override
    public double dst(IPositionTrackerImmutable _tracker)
    {
        ImmutableVector2D trackerPosition = _tracker.getPosition();

        return position.dst(trackerPosition);
    }

    @Override
    public double dst2(IPositionTrackerImmutable _tracker)
    {
        ImmutableVector2D trackerPosition = _tracker.getPosition();

        return position.dst2(trackerPosition);
    }
}
