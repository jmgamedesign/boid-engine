package boids.mind;

import vectors.ComputingVector2D;

public class OrderWorkbook
{
    public volatile double desiredMaxSpeed = 0;
    public volatile double desiredMaxSpeedTimesDeltaTime = 0;

    public volatile ComputingVector2D desiredVector = new ComputingVector2D();
    public volatile ComputingVector2D steerVector = new ComputingVector2D();
}
