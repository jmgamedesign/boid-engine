package boids.mind.cycle_ops.add_desired_vector;

import boids.IBoidImmutable;
import boids.mind.OrderWorkbook;
import boids.mind.cycle_ops.ICycleOperator;
import vectors.ImmutableVector2D;
import simulation.SimulationConfig;

public class AddCounterAlignToDesiredVector implements ICycleOperator
{
    @Override
    public boolean execute(IBoidImmutable _selfBoid, IBoidImmutable _counterBoid, double _counterDistance, OrderWorkbook _workbook, SimulationConfig _config)
    {
        if(_counterBoid == null)
        {
            _workbook.desiredVector.setZero();
            return false;
        }

        ImmutableVector2D counterVelocity = _counterBoid.getPositionTracker().getVelocity();

        _workbook.desiredVector.add(counterVelocity);

        return true;
    }
}
