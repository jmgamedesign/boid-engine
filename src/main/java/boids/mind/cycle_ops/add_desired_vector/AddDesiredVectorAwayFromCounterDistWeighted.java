package boids.mind.cycle_ops.add_desired_vector;

import boids.IBoidImmutable;
import boids.mind.OrderWorkbook;
import boids.mind.cycle_ops.ICycleOperator;
import vectors.ComputingVector2D;
import vectors.ImmutableVector2D;
import simulation.SimulationConfig;

public class AddDesiredVectorAwayFromCounterDistWeighted implements ICycleOperator
{
    @Override
    public boolean execute(IBoidImmutable _selfBoid, IBoidImmutable _counterBoid, double _counterDistance, OrderWorkbook _workbook, SimulationConfig _config)
    {
        if(_counterBoid == null)
        {
            _workbook.desiredVector.setZero();
            return false;
        }

        if (_counterDistance < .1)
        {
            _counterDistance = .1;
        }

        ImmutableVector2D selfPosition = _selfBoid.getPositionTracker().getPosition();
        ImmutableVector2D counterPosition = _counterBoid.getPositionTracker().getPosition();

        ComputingVector2D vectorAwayFromCounter = selfPosition.beginCompute().sub(counterPosition).unitVector().div(_counterDistance);

        _workbook.desiredVector.add(vectorAwayFromCounter);

        return true;
    }
}
