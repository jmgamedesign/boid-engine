package boids.mind.cycle_ops.check;

import boids.IBoidImmutable;
import boids.mind.OrderWorkbook;
import boids.mind.cycle_ops.ICycleOperator;
import simulation.SimulationConfig;

public class TeamCheck implements ICycleOperator
{
    private final int team;

    public TeamCheck(int _team)
    {
        team = _team;
    }

    @Override
    public boolean execute(IBoidImmutable _selfBoid, IBoidImmutable _counterBoid, double _counterDistance, OrderWorkbook _workbook, SimulationConfig _config)
    {
        if(_counterBoid == null)
        {
            _workbook.desiredVector.setZero();
            return false;
        }

        return _counterBoid.getTeam() == team;
    }
}
