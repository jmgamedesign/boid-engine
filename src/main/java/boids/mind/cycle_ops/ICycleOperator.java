package boids.mind.cycle_ops;

import boids.IBoidImmutable;
import boids.mind.OrderWorkbook;
import simulation.SimulationConfig;

@FunctionalInterface
public interface ICycleOperator
{
    boolean execute(IBoidImmutable _selfBoid, IBoidImmutable _counterBoid, double _counterDistance, OrderWorkbook _workbook, SimulationConfig _config);
}
