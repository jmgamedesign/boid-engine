package boids.mind.post_cycle_ops.set;

import boids.IBoidImmutable;
import boids.location_tracker.IPositionTrackerImmutable;
import boids.mind.OrderWorkbook;
import boids.mind.post_cycle_ops.IPostCycleOperator;
import vectors.ImmutableVector2D;
import simulation.SimulationConfig;

public class SetDesiredVectorTowardPositionTracker implements IPostCycleOperator
{
    private final IPositionTrackerImmutable tracker;

    public SetDesiredVectorTowardPositionTracker(IPositionTrackerImmutable _tracker)
    {
        tracker = _tracker;
    }

    @Override
    public boolean execute(IBoidImmutable _selfBoid, OrderWorkbook _workbook, SimulationConfig _config)
    {
        ImmutableVector2D selfPosition = _selfBoid.getPositionTracker().getPosition();
        ImmutableVector2D trackerPosition = tracker.getPosition();

        _workbook.desiredVector = trackerPosition.beginCompute().sub(selfPosition).unitVector();

        return true;
    }
}
