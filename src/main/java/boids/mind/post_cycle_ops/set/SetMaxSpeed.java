package boids.mind.post_cycle_ops.set;

import boids.IBoidImmutable;
import boids.mind.OrderWorkbook;
import boids.mind.post_cycle_ops.IPostCycleOperator;
import simulation.SimulationConfig;

public class SetMaxSpeed implements IPostCycleOperator
{
    private final double maxSpeed;
    private final double maxSpeedTimesDeltaTime;

    public SetMaxSpeed(double _fraction, SimulationConfig _config)
    {
        maxSpeed = _config.maxSpeed * _fraction;
        maxSpeedTimesDeltaTime = maxSpeed * _config.deltaTime;
    }

    @Override
    public boolean execute(IBoidImmutable _selfBoid, OrderWorkbook _workbook, SimulationConfig _config)
    {
        _workbook.desiredMaxSpeed = maxSpeed;
        _workbook.desiredMaxSpeedTimesDeltaTime = maxSpeedTimesDeltaTime;

        return true;
    }
}
