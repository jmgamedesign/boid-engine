package boids.mind.post_cycle_ops.steer;

import boids.IBoidImmutable;
import boids.mind.OrderWorkbook;
import boids.mind.post_cycle_ops.IPostCycleOperator;
import simulation.SimulationConfig;

public class SetOrderPriority implements IPostCycleOperator
{
    private final double priority;

    public SetOrderPriority(double _priority)
    {
        priority = _priority;
    }

    @Override
    public boolean execute(IBoidImmutable _selfBoid, OrderWorkbook _workbook, SimulationConfig _config)
    {
        _workbook.steerVector.mul(priority);

        return true;
    }
}
