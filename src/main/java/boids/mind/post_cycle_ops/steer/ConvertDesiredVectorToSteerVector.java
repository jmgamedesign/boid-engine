package boids.mind.post_cycle_ops.steer;

import boids.IBoidImmutable;
import boids.mind.OrderWorkbook;
import boids.mind.post_cycle_ops.IPostCycleOperator;
import vectors.ComputingVector2D;
import vectors.ImmutableVector2D;
import simulation.SimulationConfig;

public class ConvertDesiredVectorToSteerVector implements IPostCycleOperator
{
    @Override
    public boolean execute(IBoidImmutable _selfBoid, OrderWorkbook _workbook, SimulationConfig _config)
    {
        ImmutableVector2D velocity = _selfBoid.getPositionTracker().getVelocity();

        _workbook.steerVector = new ComputingVector2D(_workbook.desiredVector.setLength(_workbook.desiredMaxSpeedTimesDeltaTime).sub(velocity).limit(_config.maxSpeedTimesDeltaTime/2));

        return true;
    }
}
