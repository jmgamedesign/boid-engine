package boids.mind.post_cycle_ops.check;

import boids.IBoidImmutable;
import boids.mind.OrderWorkbook;
import boids.mind.post_cycle_ops.IPostCycleOperator;
import vectors.ImmutableVector2D;
import simulation.SimulationConfig;

public class ZeroDesiredVectorCheck implements IPostCycleOperator
{
    @Override
    public boolean execute(IBoidImmutable _selfBoid, OrderWorkbook _workbook, SimulationConfig _config)
    {
        //ComputingVector2Ds do not have their internal values exposed so they cannot easily be used where ImmutableVector2Ds should be used.
        ImmutableVector2D desiredVector = _workbook.desiredVector.endCompute();

        return desiredVector.x != 0 && desiredVector.y !=0;
    }
}
