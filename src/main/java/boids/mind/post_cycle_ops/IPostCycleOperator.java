package boids.mind.post_cycle_ops;

import boids.IBoidImmutable;
import boids.mind.OrderWorkbook;
import simulation.SimulationConfig;

@FunctionalInterface
public interface IPostCycleOperator
{
    boolean execute(IBoidImmutable _selfBoid, OrderWorkbook _workbook, SimulationConfig _config);
}
