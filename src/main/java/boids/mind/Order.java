package boids.mind;

import boids.location_tracker.IPositionTrackerImmutable;
import boids.mind.cycle_ops.ICycleOperator;
import boids.mind.cycle_ops.add_desired_vector.AddCounterAlignToDesiredVector;
import boids.mind.cycle_ops.add_desired_vector.AddDesiredVectorAwayFromCounterDistWeighted;
import boids.mind.cycle_ops.add_desired_vector.AddDesiredVectorTowardCounter;
import boids.mind.cycle_ops.add_desired_vector.AddDesiredVectorTowardCounterDistWeighted;
import boids.mind.cycle_ops.check.NearCheckSpace;
import boids.mind.cycle_ops.check.TeamCheck;
import boids.mind.post_cycle_ops.IPostCycleOperator;
import boids.mind.post_cycle_ops.check.ZeroDesiredVectorCheck;
import boids.mind.post_cycle_ops.set.SetDesiredVectorTowardPositionTracker;
import boids.mind.post_cycle_ops.set.SetMaxSpeed;
import boids.mind.post_cycle_ops.steer.ConvertDesiredVectorToSteerVector;
import boids.mind.post_cycle_ops.steer.SetOrderPriority;
import simulation.SimulationConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Order
{
    private final List<ICycleOperator> cycleProgram;
    private final List<IPostCycleOperator> postCycleProgram;
    public final String name;

    public Order(List<ICycleOperator> _cycleProgram, List<IPostCycleOperator> _postCycleProgram, String _name)
    {
        cycleProgram = Collections.unmodifiableList(new ArrayList<>(_cycleProgram));
        postCycleProgram = Collections.unmodifiableList(new ArrayList<>(_postCycleProgram));

        name = _name;
    }

    public OrderExecutor getExecutor()
    {
        return new OrderExecutor(cycleProgram, postCycleProgram, name);
    }

    public static Order basicSeparate(double _speedPercent, double _priority, SimulationConfig _config)
    {
        List<ICycleOperator> cycleProgram = new ArrayList<>();
        List<IPostCycleOperator> postCycleProgram = new ArrayList<>();

        cycleProgram.add(new NearCheckSpace());
        cycleProgram.add(new AddDesiredVectorAwayFromCounterDistWeighted());

        postCycleProgram.add(new ZeroDesiredVectorCheck());
        postCycleProgram.add(new SetMaxSpeed(_speedPercent, _config));
        postCycleProgram.add(new ConvertDesiredVectorToSteerVector());
        postCycleProgram.add(new SetOrderPriority(_priority / 100));

        return new Order(cycleProgram, postCycleProgram, "basicSeparate");
    }

    public static Order basicCohere(int _team, double _speedPercent, double _priority, SimulationConfig _config)
    {
        List<ICycleOperator> cycleProgram = new ArrayList<>();
        List<IPostCycleOperator> postCycleProgram = new ArrayList<>();

        cycleProgram.add(new TeamCheck(_team));
        cycleProgram.add(new AddDesiredVectorTowardCounter());

        postCycleProgram.add(new ZeroDesiredVectorCheck());
        postCycleProgram.add(new SetMaxSpeed(_speedPercent, _config));
        postCycleProgram.add(new ConvertDesiredVectorToSteerVector());
        postCycleProgram.add(new SetOrderPriority(_priority / 100));

        return new Order(cycleProgram, postCycleProgram, "basicCohere");
    }

    public static Order basicAlign(int _team, double _speedPercent, double _priority, SimulationConfig _config)
    {
        List<ICycleOperator> cycleProgram = new ArrayList<>();
        List<IPostCycleOperator> postCycleProgram = new ArrayList<>();

        cycleProgram.add(new TeamCheck(_team));
        cycleProgram.add(new AddCounterAlignToDesiredVector());

        postCycleProgram.add(new ZeroDesiredVectorCheck());
        postCycleProgram.add(new SetMaxSpeed(_speedPercent, _config));
        postCycleProgram.add(new ConvertDesiredVectorToSteerVector());
        postCycleProgram.add(new SetOrderPriority(_priority / 100));

        return new Order(cycleProgram, postCycleProgram, "basicAlign");
    }

    public static Order basicGravitate(IPositionTrackerImmutable _location, double _speedPercent, double _priority, SimulationConfig _config)
    {
        List<ICycleOperator> cycleProgram = new ArrayList<>();
        List<IPostCycleOperator> postCycleProgram = new ArrayList<>();

        postCycleProgram.add(new SetDesiredVectorTowardPositionTracker(_location));
        postCycleProgram.add(new SetMaxSpeed(_speedPercent, _config));
        postCycleProgram.add(new ConvertDesiredVectorToSteerVector());
        postCycleProgram.add(new SetOrderPriority(_priority / 100));

        return new Order(cycleProgram, postCycleProgram, "basicGravitate");
    }

    public static Order basicHate(int _team, double _speedPercent, double _priority, SimulationConfig _config)
    {
        List<ICycleOperator> cycleProgram = new ArrayList<>();
        List<IPostCycleOperator> postCycleProgram = new ArrayList<>();

        cycleProgram.add(new TeamCheck(_team));
        cycleProgram.add(new AddDesiredVectorAwayFromCounterDistWeighted());

        postCycleProgram.add(new ZeroDesiredVectorCheck());
        postCycleProgram.add(new SetMaxSpeed(_speedPercent, _config));
        postCycleProgram.add(new ConvertDesiredVectorToSteerVector());
        postCycleProgram.add(new SetOrderPriority(_priority / 100));

        return new Order(cycleProgram, postCycleProgram, "basicHate");
    }

    public static Order basicFight(int _team, double _speedPercent, double _priority, SimulationConfig _config)
    {
        List<ICycleOperator> cycleProgram = new ArrayList<>();
        List<IPostCycleOperator> postCycleProgram = new ArrayList<>();

        cycleProgram.add(new TeamCheck(_team));
        cycleProgram.add(new AddDesiredVectorTowardCounterDistWeighted());

        postCycleProgram.add(new ZeroDesiredVectorCheck());
        postCycleProgram.add(new SetMaxSpeed(_speedPercent, _config));
        postCycleProgram.add(new ConvertDesiredVectorToSteerVector());
        postCycleProgram.add(new SetOrderPriority(_priority / 100));

        return new Order(cycleProgram, postCycleProgram, "basicFight");
    }

    public static Order emptyOrder()
    {
        List<ICycleOperator> cycleProgram = new ArrayList<>();
        List<IPostCycleOperator> postCycleProgram = new ArrayList<>();

        return new Order(cycleProgram, postCycleProgram, "empty");
    }

    @Override
    public String toString()
    {
        String cycle = "Cycle Program: ";
        String postCycle = " Post Cycle Program: ";

        for (ICycleOperator operator : cycleProgram)
        {
            cycle = cycle + operator.getClass().getSimpleName() + " ";
        }

        for (IPostCycleOperator operator : postCycleProgram)
        {
            postCycle = postCycle + operator.getClass().getSimpleName() + " ";
        }

        return cycle + "\n" + postCycle;
    }
}
