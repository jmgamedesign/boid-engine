package boids.mind;

import boids.IBoidImmutable;
import boids.mind.cycle_ops.ICycleOperator;
import boids.mind.post_cycle_ops.IPostCycleOperator;
import simulation.SimulationConfig;
import vectors.ComputingVector2D;

import java.util.List;

public class OrderExecutor
{
    private final List<ICycleOperator> cycleProgram;
    private final List<IPostCycleOperator> postCycleProgram;
    public final String name;

    private volatile OrderWorkbook workbook = new OrderWorkbook();

    OrderExecutor(List<ICycleOperator> _cycleProgram, List<IPostCycleOperator> _postCycleProgram, String _name)
    {
        cycleProgram = _cycleProgram;
        postCycleProgram =  _postCycleProgram;

        name = _name;
    }

    public void executeCycle(IBoidImmutable _selfBoid, IBoidImmutable _counterBoid, double _counterDistance, SimulationConfig _config)
    {
        if (_selfBoid == _counterBoid)
        {
            return;
        }

        for(int i = 0; i < cycleProgram.size(); i++)
        {
            ICycleOperator operator = cycleProgram.get(i);

            if (!operator.execute(_selfBoid, _counterBoid, _counterDistance, workbook, _config))
            {
                break;
            }
        }
    }

    public ComputingVector2D executePostCycle(IBoidImmutable _selfBoid, SimulationConfig _config)
    {
        for(int i = 0; i < postCycleProgram.size(); i++)
        {
            IPostCycleOperator operator = postCycleProgram.get(i);
            if (!operator.execute(_selfBoid, workbook, _config))
            {
                break;
            }
        }

        return workbook.steerVector;
    }
}
